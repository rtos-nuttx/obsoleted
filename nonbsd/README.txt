README
======

This repository exists to isolate NuttX components that are incompatible
with the NuttX 3-clause BSD license.  This includes software with
proprietary or GPL licensing.

Users who decide to incorporate software with these viral license must
understand the impact of inclusion of the software; the resulting NuttX
binaries will need to conform to the licensing of any modules installed from
this directory.

